// @flow

import ReactDOM from "react-dom";

import { ViewComponent } from "./application/view/viewComponents";

const rootNode = document.getElementById("root");

ReactDOM.render(<ViewComponent />, rootNode);
