// @flow

import * as React from "react";

type Props = {};

export const ViewComponent: React.AbstractComponent<Props> = () => (
  <h1>My application</h1>
);
